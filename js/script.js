"use strict";
/*
JavaScript - однопоточна мова програмування, код виконується в одному стеку викликів один за одним, від рядка до рядка. Асинхронність у JavaScript - це виконання скриптів не в тому порядку, як вони написані, а із заданою програмно послідовністю, завдяки включеним в код командам, що забезпечують затримки та очікування.
*/

const ipUrl = `https://api.ipify.org/?format=json`;
const spotUrl = 'http://ip-api.com/json/';
const getIp = async(ipUrl) => {
    const ipObj = await fetch(ipUrl).then(res => res.json());
    const {ip} = ipObj;
    return ip;
}
(async() => console.log(`IP: ${await getIp(ipUrl)}`))()
class Pin {
    constructor({continent, country, regionName, city, zip}){
        this.continent = continent;
        this.country = country;
        this.regionName = regionName;
        this.city = city;
        this.zip = zip;
    }
    render(selector) {
        document.querySelector(selector).innerHTML = `
				    <p class="spot">Continent: ${this.continent}</p>
				    <p class="spot">Country: ${this.country}</p>
				    <p class="spot">Region: ${this.regionName}</p>
				    <p class="spot">City: ${this.city}</p>
				    <p class="spot">District (Postcode): ${this.zip}</p>`;
    }
}
const showLocation = async() => {
    const spotResponse = await fetch(`${spotUrl}${await getIp(ipUrl)}?fields=status,continent,country,regionName,city,zip,query`).then(res => res.json());
    const {continent, country, regionName, city, zip, status} = spotResponse;
    if(status === 'success'){
        new Pin({continent, country, regionName, city, zip}).render('div.pin');
    }
}
document.querySelector('button.button').addEventListener('click', showLocation)